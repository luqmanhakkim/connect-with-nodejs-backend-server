import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:try1/model.dart';

/// use the property of the future and call the fetchPost that been made in the Future function
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Test'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  const MyHomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: FutureBuilder<List<User>>(
        future: fetchUser(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? UserList(user: snapshot.data)
              : Center(
                  child: CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}

class UserList extends StatelessWidget {
  final List<User> user;

  const UserList({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
        itemCount: user.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: Card(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.directions_bike),
                 Padding(padding: EdgeInsets.only(left:50.0),),
                  Column(
                    children: <Widget>[
                      Text('${user[index].name}',textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold),),
                      Text('${user[index].address}',style: TextStyle(color: Colors.grey),),
                      Text('${user[index].email}'),
                      Text('${user[index].password}')
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }
}
