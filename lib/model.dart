import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

List<User> parseUser(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<User>((json) => User.fromJson(json)).toList();
}

Future<List<User>> fetchUser(http.Client client) async {
  final response = await client.get('http://192.168.103.236:4200/api/user');
  return compute(parseUser, response.body);
}

class User {
  int userId;
  String name;
  String address;
  String email;
  String password;

  User({
    this.userId,
    this.name,
    this.address,
    this.email,
    this.password,
  });

  factory User.fromJson(Map<String, dynamic> json) => new User(
        userId: json["user_id"],
        name: json["name"],
        address: json["address"],
        email: json["email"],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "name": name,
        "address": address,
        "email": email,
        "password": password,
      };
}
